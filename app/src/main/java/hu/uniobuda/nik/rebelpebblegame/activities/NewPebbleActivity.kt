package hu.uniobuda.nik.rebelpebblegame.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatCheckBox
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.AppCompatTextView
import android.util.Log
import hu.uniobuda.nik.rebelpebblegame.R
import android.widget.ArrayAdapter
import android.widget.Spinner
import hu.uniobuda.nik.rebelpebblegame.classes.PebbleEntity
import java.time.LocalDateTime
import java.util.*
import kotlin.math.log

class NewPebbleActivity : AppCompatActivity() {

    val random : Random = Random()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newpebble)

        val dropdown = findViewById<Spinner>(R.id.eyecolorinput)
        val items = arrayOf("Sea blue", "Crimson red", "Like Steve Buscemi's", "Just green", "U235")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, items)
        dropdown.adapter = adapter

        val proceedButton = findViewById<AppCompatButton>(R.id.proceedButton);
        proceedButton.setOnClickListener {
            val intent = Intent(this, PebbleStatusActivity::class.java)

            var name : String = findViewById<AppCompatEditText>(R.id.nameinput).getText().toString()
            if (name == "") {
                name = "Rocky"
            }
            var eyecolor : String = findViewById<Spinner>(R.id.eyecolorinput).selectedItem.toString()
            var likessports : Boolean = findViewById<AppCompatCheckBox>(R.id.sportsinput).isChecked
            var likesbadmovies : Boolean = findViewById<AppCompatCheckBox>(R.id.badmoviesinput).isChecked
            var ismoody : Boolean = !findViewById<AppCompatCheckBox>(R.id.rockbeforeinput).isChecked
            var parentsingle : Boolean = findViewById<AppCompatCheckBox>(R.id.singleinput).isChecked

            var affection : Int = 50
            if (parentsingle){
                affection -= 10
            }
            var now : Date = Calendar.getInstance().time

            var pebblebody : Int = random.nextInt(5) + 1
            var pebblemouth : Int = random.nextInt(2) + 1

            var pebble : PebbleEntity = PebbleEntity(
                name,
                eyecolor,
                likessports,
                likesbadmovies,
                ismoody,
                0,
                affection,
                now.time,
                90,
                80,
                80,
                pebblebody,
                pebblemouth,
                now.time
            )
            pebble.SavePebble()

            startActivity(intent)
            finish()
        }

        val sportsText = findViewById<AppCompatTextView>(R.id.sportsinputtext);
        sportsText.setOnClickListener {
            val checkbox = findViewById<AppCompatCheckBox>(R.id.sportsinput)
            checkbox.isChecked = !checkbox.isChecked
        }

        val badMovieText = findViewById<AppCompatTextView>(R.id.badmoviesinputtext);
        badMovieText.setOnClickListener {
            val checkbox = findViewById<AppCompatCheckBox>(R.id.badmoviesinput)
            checkbox.isChecked = !checkbox.isChecked
        }

        val rockBeforeText = findViewById<AppCompatTextView>(R.id.rockbeforeinputtext);
        rockBeforeText.setOnClickListener {
            val checkbox = findViewById<AppCompatCheckBox>(R.id.rockbeforeinput)
            checkbox.isChecked = !checkbox.isChecked
        }

        val singleText = findViewById<AppCompatTextView>(R.id.singleinputtext);
        singleText.setOnClickListener {
            val checkbox = findViewById<AppCompatCheckBox>(R.id.singleinput)
            checkbox.isChecked = !checkbox.isChecked
        }
    }
}