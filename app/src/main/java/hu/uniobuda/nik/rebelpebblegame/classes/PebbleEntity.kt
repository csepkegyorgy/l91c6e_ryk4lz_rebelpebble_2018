package hu.uniobuda.nik.rebelpebblegame.classes

import android.os.Environment
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.lang.Exception
import java.util.*

class PebbleEntity(
    var n : String,
    var ec : String = "Sea blue",
    var ls : Boolean = false,
    var lbm : Boolean = false,
    var im : Boolean = false,
    var ag : Int = 0,
    var af : Int = 50,
    var lco : Long = 0,
    var l : Int = 80,
    var f : Int = 80,
    var h : Int = 80,
    var pb : Int = 1,
    var m : Int = 2,
    var bd : Long = 0
){
    companion object {
        fun PebbleExists() : Boolean{
            var file : File = File(Environment.getExternalStorageDirectory().toString() + "/pebbledata.txt")
            return file.exists()
        }

        fun LoadPebble() : PebbleEntity{
            var file : File = File(Environment.getExternalStorageDirectory().toString() + "/pebbledata.txt")
            var fr = FileReader(file)
            var dataLine : List<String> = fr.readText().split(";")

            var name : String = dataLine[0]
            var eyecolor : String = dataLine[1]
            var likessports : Boolean = dataLine[2].toBoolean()
            var likesbadmovies : Boolean = dataLine[3].toBoolean()
            var ismoody : Boolean = dataLine[4].toBoolean()
            var age : Int = dataLine[5].toInt()
            var affection : Int = dataLine[6].toInt()
            //var lcoLine : List<String> = dataLine[7].split("-")
            var lastcheckedon : Long = dataLine[7].toLong()
            var love : Int = dataLine[8].toInt()
            var food : Int = dataLine[9].toInt()
            var hygiene : Int = dataLine[10].toInt()
            var pebblebody : Int = dataLine[11].toInt()
            var mouth : Int = dataLine[12].toInt()
            //var bdLine : List<String> = dataLine[13].split("-")
            var birthdate : Long = dataLine[13].toLong()

            fr.close()

            return PebbleEntity(
                name,
                eyecolor,
                likessports,
                likesbadmovies,
                ismoody,
                age,
                affection,
                lastcheckedon,
                love,
                food,
                hygiene,
                pebblebody,
                mouth,
                birthdate
            )
        }

        fun DeletePebble() {
            var file : File = File(Environment.getExternalStorageDirectory().toString() + "/pebbledata.txt")
            var tries : Int = 3
            while (file.exists() && tries > 0){
                file.delete()
                tries--
                Thread.sleep(1000)
            }
        }
    }

    var Name : String = n
    var EyeColor : String = ec
    var LikesSports : Boolean = ls
    var LikesBadMovies : Boolean = lbm
    var IsMoody : Boolean = im
    var Age : Int = ag
    var Affection : Int = af
    var LastCheckedOn : Long = lco
    var Love : Int = l
    var Food : Int = f
    var Hygiene : Int = h
    var Body : Int = pb
    var Mouth : Int = m
    var BirthDate : Long = bd

    override fun toString() : String{

        var out : String = ""

        out += this.Name
        out += ";"
        out += this.EyeColor
        out += ";"
        out += this.LikesSports.toString()
        out += ";"
        out += this.LikesBadMovies.toString()
        out += ";"
        out += this.IsMoody.toString()
        out += ";"
        out += this.Age.toString()
        out += ";"
        out += this.Affection.toString()
        out += ";"
        out += this.LastCheckedOn.toString()
        out += ";"
        out += this.Love.toString()
        out += ";"
        out += this.Food.toString()
        out += ";"
        out += this.Hygiene.toString()
        out += ";"
        out += this.Body.toString()
        out += ";"
        out += this.Mouth.toString()
        out += ";"
        out += this.BirthDate.toString()

        return out
    }

    fun SavePebble() : String{
        try{
            var file : File = File(Environment.getExternalStorageDirectory().toString() + "/pebbledata.txt")
            var fw = FileWriter(file)

            var rawPebbleString : String = this.toString()

            fw.write(rawPebbleString)

            fw.close()
            return Environment.getExternalStorageDirectory().toString() + "/pebbledata.txt"
        }
        catch (e: Exception){
            return e.toString()
        }
    }

    fun GetModifiedAffection() : Int {
        var loveModifier : Int = 0
        var foodModifier : Int = 0
        var hygieneModifier : Int = 0

        if (this.Love > 80 && this.Food > 80 && this.Hygiene > 80){
            loveModifier = this.Love - 80
            foodModifier = this.Food - 80
            hygieneModifier = this.Hygiene - 80
        }
        else{
            if (this.Love < 50){
                loveModifier = -1 * (50 - this.Love)
            }

            if (this.Food < 50){
                loveModifier = -1 * (50 - this.Food)
            }

            if (this.Hygiene < 50){
                loveModifier = -1 * (50 - this.Hygiene)
            }
        }

        var finalAffection : Int = this.Affection + loveModifier + foodModifier + hygieneModifier
        if (finalAffection < 0){
            finalAffection = 0
        }
        if (finalAffection > 100){
            finalAffection = 100
        }

        return finalAffection
    }

    fun Update() {
        var random : Random = Random()
        // Day in millisecs : 86400000
        // Hour in millisecs : 3600000
        var now : Long = Calendar.getInstance().time.time
        var daysPassed = (now - this.BirthDate) / 86400000
        this.Age = daysPassed.toInt()

        var hoursPassedSinceLco : Int = ((now - this.LastCheckedOn) / 3600000).toInt()
        this.Love -= hoursPassedSinceLco * (random.nextInt(10) + 5)
        if (this.Love < 0){
            this.Love = 0
        }
        this.Food -= hoursPassedSinceLco * (random.nextInt(10) + 5)
        if (this.Food < 0){
            this.Food = 0
        }
        this.Hygiene -= hoursPassedSinceLco * (random.nextInt(10) + 5)
        if (this.Hygiene < 0){
            this.Hygiene = 0
        }
    }

    fun SetLastCheckedOn() {
        this.LastCheckedOn = Calendar.getInstance().time.time
    }
}