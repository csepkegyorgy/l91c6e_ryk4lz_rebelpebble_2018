package hu.uniobuda.nik.rebelpebblegame.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.AppCompatButton
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import hu.uniobuda.nik.rebelpebblegame.R
import hu.uniobuda.nik.rebelpebblegame.classes.PebbleEntity
import hu.uniobuda.nik.rebelpebblegame.fragments.InfoFragment
import java.util.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var permissionCheckWrite : Int = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheckWrite != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        }
        if (permissionCheckWrite != PackageManager.PERMISSION_GRANTED) {
            System.exit(0)
        }

        var permissionCheckRead : Int = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
        if (permissionCheckRead != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        }
        if (permissionCheckRead != PackageManager.PERMISSION_GRANTED) {
            System.exit(0)
        }
    }

    override fun onStart() {
        super.onStart()
        if (PebbleEntity.PebbleExists()){
            var pebble = PebbleEntity.LoadPebble()
            pebble.Update()
            pebble.SavePebble()
            if (pebble.Age >= 10) {
                val intent = Intent(this, PebbleResultActivity::class.java)
                startActivity(intent)
                finish()
            }
            else {
                val intent = Intent(this, PebbleStatusActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

        val newPebbleButton = findViewById<AppCompatButton>(R.id.newPebbleButton);

        newPebbleButton.setOnClickListener {
            val intent = Intent(this, NewPebbleActivity::class.java)
            startActivity(intent)
            finish()
        }

        val infoButton = findViewById<AppCompatButton>(R.id.infoButton);
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val fragment = InfoFragment()

        infoButton.setOnClickListener {
            fragmentTransaction.add(R.id.fragment_container, fragment)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        }
        val buttonExit = findViewById<AppCompatButton>(R.id.buttonExit)

        buttonExit.setOnClickListener {
            System.exit(0)
        }
    }
}
