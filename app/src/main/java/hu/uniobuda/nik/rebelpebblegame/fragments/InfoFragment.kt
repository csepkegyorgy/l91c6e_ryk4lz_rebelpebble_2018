package hu.uniobuda.nik.rebelpebblegame.fragments

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import hu.uniobuda.nik.rebelpebblegame.R
import kotlinx.android.synthetic.main.infofragment.*

class InfoFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        //Inflate the layout for this fragment
        return inflater.inflate(
                R.layout.infofragment, container, false)


    }


}