package hu.uniobuda.nik.rebelpebblegame.activities

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import hu.uniobuda.nik.rebelpebblegame.R
import hu.uniobuda.nik.rebelpebblegame.classes.PebbleEntity
import java.util.*

class PebbleResultActivity : AppCompatActivity() {

    lateinit var pebble : PebbleEntity
    lateinit var reasonsSplit : MutableList<String>
    var random : Random = Random()
    var finalProfession : String = ""
    var pebbleResultId : Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pebbleresult)

        pebble = PebbleEntity.LoadPebble()

        var finalPoints : Int = EvaluatePebble()

        if (finalPoints <= 50) {
            finalProfession = "Stoner"
            reasonsSplit.add("He likes to take a break.")
            pebbleResultId = 1
        }
        else if (finalPoints > 50 && finalPoints <= 100) {
            finalProfession = "Android developer"
            reasonsSplit.add("He was desperate and needed the money.")
            pebbleResultId = 2
        }
        else if (finalPoints > 100 && finalPoints <= 150) {
            if (random.nextBoolean() == true) {
                finalProfession = "Rock hauler"
                reasonsSplit.add("He barely found a job.")
                pebbleResultId = 3
            }
            else {
                finalProfession = "Rolling Stone"
                reasonsSplit.add("He had a great taste in music.")
                pebbleResultId = 4
            }
        }
        else {
            if (pebble.LikesBadMovies) {
                finalProfession = "Rock"
                reasonsSplit.add("He is a terrible actor.")
                pebbleResultId = 5
            }
            else {
                finalProfession = "BoulDermatologist"
                reasonsSplit.add("He can quickly get under your skin.")
                pebbleResultId = 6
            }
        }

        SetupView()
        PebbleEntity.DeletePebble()
    }

    private fun EvaluatePebble() : Int{
        var finalAffection : Int = pebble.GetModifiedAffection()
        var points : Int = finalAffection
        var reasons : String = ""

        if (Calendar.getInstance().time.time - pebble.LastCheckedOn <= 7200000) { // if pebble was checked in the last two hours
            points += 20
            reasons += "You've been there for him just the right moment.;"
        } else if (Calendar.getInstance().time.time - pebble.LastCheckedOn <= 36000000) { // if pebble was checked in the last two hours
            points += 10
            reasons += "You've been there for him near the time it was needed.;"
        }

        if (finalAffection > 80 && pebble.IsMoody) {
            points += 30
            reasons += "He had a good example.;"
        }
        else if ((finalAffection < 80 && finalAffection > 50) && pebble.IsMoody) {
            points += 10
            reasons += "He grew over his bad mood, and managed to be happy.;"
        }

        else if (finalAffection < 50 && pebble.IsMoody) {
            points -= 10
            reasons += "He couldn't handle his demons.;"
        }

        if (pebble.EyeColor == "Like Steve Buscemi's") {
            points += 10
            reasons += "He had eyes of an angel.;"
        }

        if (pebble.Love > 80) {
            points += 10
            reasons += "He was always loved;"
        }
        else {
            points -= 10
            reasons += "He was often neglected.;"
        }

        if (pebble.Mouth == 1) {
            points -= 10
            reasons += "He had a weird mouth.;"
        }

        if (pebble.Body == 5) {
            points -= 10
            reasons += "He had a charming figure.;"
        }

        if (pebble.Food < 40) {
            points -= 10
            reasons += "He grew weak of you ignoring his hunger.;"
        }

        if (pebble.Hygiene > 80) {
            points += 10
            reasons += "He has a charming smell.;"
        }

        reasons = reasons.dropLast(1)
        reasonsSplit = reasons.split(";").toMutableList()

        reasonsSplit.add("He was kind of born for it.")
        reasonsSplit.add("He has an attitude.")
        reasonsSplit.add("He does it really well.")

        return points
    }

    private fun SetupView(){

        val backButton = findViewById<AppCompatButton>(R.id.mainpage_button)
        backButton.setOnClickListener() {
            val builder =
                AlertDialog.Builder(this@PebbleResultActivity)
            builder.setTitle("Back to Main Page")
            builder.setMessage("Are you sure?")
            builder.setPositiveButton("OK") { dialog, which ->
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()

            }
            builder.setNegativeButton("Cancel") { dialog, which ->

            }
            val dialog : AlertDialog = builder.create()
            dialog.show()
        }

        val finalProfessionText = findViewById<AppCompatTextView>(R.id.finalProfession)
        finalProfessionText.setText(finalProfession)

        var finalReasonsText : String = ""
        var randomIndex : Int = 0

        randomIndex = random.nextInt(reasonsSplit.size)
        finalReasonsText += " - "
        finalReasonsText += reasonsSplit[randomIndex]
        finalReasonsText += "\n"
        reasonsSplit.removeAt(randomIndex)

        randomIndex = random.nextInt(reasonsSplit.size)
        finalReasonsText += " - "
        finalReasonsText += reasonsSplit[randomIndex]
        finalReasonsText += "\n"
        reasonsSplit.removeAt(randomIndex)

        randomIndex = random.nextInt(reasonsSplit.size)
        finalReasonsText += " - "
        finalReasonsText += reasonsSplit[randomIndex]
        finalReasonsText += "\n"
        reasonsSplit.removeAt(randomIndex)

        randomIndex = random.nextInt(reasonsSplit.size)
        finalReasonsText += " - "
        finalReasonsText += reasonsSplit[randomIndex]
        finalReasonsText += "\n"
        reasonsSplit.removeAt(randomIndex)

        randomIndex = random.nextInt(reasonsSplit.size)
        finalReasonsText += " - "
        finalReasonsText += reasonsSplit[randomIndex]
        reasonsSplit.removeAt(randomIndex)

        val resultReasonsText = findViewById<AppCompatTextView>(R.id.resultReasons)
        resultReasonsText.setText(finalReasonsText)

        var finalPebbleImage = findViewById<AppCompatImageView>(R.id.finalPebbleImage)
        finalPebbleImage.setImageResource(GetResultImageById(pebbleResultId))

        if (pebble.LikesSports){
            var baseballImage = findViewById<AppCompatImageView>(R.id.baseballImage)
            baseballImage.setImageResource(GetBaseballImage())
        }
    }

    fun GetResultImageById(id : Int) : Int{
        return resources.getIdentifier(("pebbleresult" + id), "drawable", packageName)
    }

    fun GetBaseballImage() : Int{
        return resources.getIdentifier("baseball", "drawable", packageName)
    }
}