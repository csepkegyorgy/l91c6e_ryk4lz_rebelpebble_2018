package hu.uniobuda.nik.rebelpebblegame.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.ProgressBar
import android.widget.Toast
import hu.uniobuda.nik.rebelpebblegame.R
import hu.uniobuda.nik.rebelpebblegame.classes.PebbleEntity
import java.lang.reflect.Field
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.RandomAccess

class PebbleStatusActivity : AppCompatActivity() {

    lateinit var pebble : PebbleEntity
    var random : Random = Random()
    var isBeingRedirectedToMainActivity : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pebblestatus)
        pebble = PebbleEntity.LoadPebble()

        val thread = object : Thread() {

            override fun run() {
                try {
                    while (!this.isInterrupted) {
                        Thread.sleep(100)
                        runOnUiThread {
                            UpdateUIStatus()
                        }
                    }
                } catch (e: InterruptedException) {
                }

            }
        }
        thread.start()

    }

    fun UpdateUIStatus(){
        val pebbleBodyImage = findViewById<AppCompatImageView>(R.id.pebbleBodyImage)
        pebbleBodyImage.setImageResource(GetBodyImageById(pebble.Body))
        val pebbleMouthImage = findViewById<AppCompatImageView>(R.id.pebbleMouthImage)
        pebbleMouthImage.setImageResource(GetMouthImageById(pebble.Mouth, pebble.GetModifiedAffection()))
        val pebbleLeftEyeImage = findViewById<AppCompatImageView>(R.id.pebbleEyeLeftImage)
        pebbleLeftEyeImage.setImageResource(GetEyecolorImageById(pebble.EyeColor))
        val pebbleRightEyeImage = findViewById<AppCompatImageView>(R.id.pebbleEyeRightImage)
        pebbleRightEyeImage.setImageResource(GetEyecolorImageById(pebble.EyeColor))

        val progressLove = findViewById<ProgressBar>(R.id.happinessProgressbar)
        progressLove.progress = pebble.Love
        val progressFood = findViewById<ProgressBar>(R.id.hungerProgressbar)
        progressFood.progress = pebble.Food
        val progressHygiene = findViewById<ProgressBar>(R.id.hygeneProgressbar)
        progressHygiene.progress = pebble.Hygiene

        val ageText = findViewById<AppCompatTextView>(R.id.ageTextStatus)
        ageText.setText("Age " + pebble.Age.toString())

        val nameText = findViewById<AppCompatTextView>(R.id.nameText)
        nameText.setText(pebble.Name)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()

        pebble = PebbleEntity.LoadPebble()

        pebble.Love += random.nextInt(5) + 3
        if (pebble.Love > 100){
            pebble.Love = 100
        }

        pebble.Update()
        pebble.SetLastCheckedOn()
        UpdateUIStatus()
    }

    override fun onPause() {
        super.onPause()

        if (!isBeingRedirectedToMainActivity){
            pebble.SavePebble()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item!!.itemId == R.id.killPebbleItem) {
            val builder=
                    AlertDialog.Builder(this@PebbleStatusActivity)
            builder.setTitle("Kill Pebble")
            builder.setMessage("Are you throwing your pebble into the sea?")
            builder.setPositiveButton("YES"){dialog, which ->
                PebbleEntity.DeletePebble()
                isBeingRedirectedToMainActivity = true
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()

            }
            builder.setNegativeButton("NO"){dialog, which ->

            }
            val dialog : AlertDialog = builder.create()
            dialog.show()
        }
        if(item!!.itemId == R.id.exitItem) {
            val builder =
                    AlertDialog.Builder(this@PebbleStatusActivity)
            builder.setTitle("Exit Confirmation")
            builder.setMessage("Are you sure you want to exit?")
            builder.setPositiveButton("YES"){dialog, which ->
                System.exit(0);
            }
            builder.setNegativeButton("NO"){dialog, which ->

            }
            val dialog : AlertDialog = builder.create()
            dialog.show()
        }
        if(item!!.itemId == R.id.hungerItem) {
            var foodpoints = random.nextInt(10) + 3
            if (pebble.Food + foodpoints > 100){
                var affectionReduction = pebble.Food + foodpoints - 100
                if (pebble.Affection - affectionReduction < 0){
                    pebble.Affection = 0
                }
                else{
                    pebble.Affection -= affectionReduction
                }
                pebble.Food = 100
                toast("Oh no you've overfed your pebble, it's sad!")
            }
            else{
                pebble.Food += foodpoints
                toast("Your pebble is getting a bellyful with +" + foodpoints + " food points!")
            }
        }
        if(item!!.itemId == R.id.hygeneItem) {
            var washpoints = random.nextInt(10) + 3
            if (pebble.Hygiene + washpoints > 100){
                var affectionReduction = pebble.Hygiene + washpoints - 100
                if (pebble.Affection - affectionReduction < 0){
                    pebble.Affection = 0
                }
                else{
                    pebble.Affection -= affectionReduction
                }
                pebble.Hygiene = 100
                toast("Oh no you've overwashed your pebble, it's sad!")
            }
            else{
                pebble.Hygiene += washpoints
                toast("Your pebble is getting clean with +" + washpoints + " hygiene points!")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun Context.toast(message: CharSequence) =
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    fun GetBodyImageById(id : Int) : Int{
        return resources.getIdentifier(("pebble" + id), "drawable", packageName)
    }

    fun GetEyecolorImageById(eyecolor : String) : Int{
        var eyecolorString : String = ""
        if (eyecolor == "Sea blue"){
            eyecolorString = "seablue"
        }
        if (eyecolor == "Crimson red"){
            eyecolorString = "crimsonred"
        }
        if (eyecolor == "Like Steve Buscemi's"){
            eyecolorString = "likestevebuscemis"
        }
        if (eyecolor == "Just green"){
            eyecolorString = "justgreen"
        }
        if (eyecolor == "U235"){
            eyecolorString = "u235"
        }
        return resources.getIdentifier(("eye" + "_" + eyecolorString), "drawable", packageName)
    }

    fun GetMouthImageById(id : Int, affection : Int) : Int{
        var affectionMoodString : String = ""
        if (affection < 50){
            affectionMoodString = "sad"
        }
        if (affection >= 50 && affection <= 80){
            affectionMoodString = "normal"
        }
        if (affection > 80){
            affectionMoodString = "happy"
        }
        return resources.getIdentifier(("mouth" + id + "_" + affectionMoodString), "drawable", packageName)
    }
}